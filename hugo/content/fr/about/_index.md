---
title: "A propos"
description: "Il y a quelques années, alors qu'il visitait, ou plutot, qu'il fouillait Notre-Dame, l'auteur de ce livre découvrit, dans un recoin obscur de l'une des tours, le mot suivant, gravé à la main au dessus d'un mur: -ANANKE."
featured_image: ''
---
{{< figure src="/images/Victor_Hugo-Hunchback.jpg" title="Illustration from Victor Hugo et son temps (1881)" >}}

_The Hunchback of Notre-Dame_ (French: _Notre-Dame de Paris_) is a French Romantic/Gothic novel by Victor Hugo, published in 1831. The original French title refers to Notre Dame Cathedral, on which the story is centered. English translator Frederic Shoberl named the novel The Hunchback of Notre Dame in 1833 because at the time, Gothic novels were more popular than Romance novels in England. The story is set in Paris, France in the Late Middle Ages, during the reign of Louis XI.
