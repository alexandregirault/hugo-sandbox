resource "aws_s3_bucket" "hugo-sandbox" {
  bucket = "hugo-sandbox"
  acl    = "private"

  tags = {
    Name        = "hugo-sandbox"
    Environment = "Prod"
  }
}

data "aws_iam_policy_document" "OAI-allow" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.hugo-sandbox.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = ["${aws_cloudfront_origin_access_identity.CF-OAI.iam_arn}"]
    }
  }
}

resource "aws_s3_bucket_policy" "CF-OAI" {
  bucket = "${aws_s3_bucket.hugo-sandbox.id}"
  policy = "${data.aws_iam_policy_document.OAI-allow.json}"
}