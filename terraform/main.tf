provider "aws" {
  region = "eu-west-3"
  version = "3.64.0"
}

provider "aws" {
  alias = "us"
  region = "us-east-1"
  version = "3.64.0"
}

terraform {
  backend "s3" {
    bucket         = "tf-states-labawsalex"
    key            = "global/s3/terraform-hugo-sandbox.state"
    region         = "eu-west-3"
    dynamodb_table = "terraforms_locks"
    encrypt        = true
  }
  required_version = "1.0.0"
}
