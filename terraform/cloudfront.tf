
data "aws_acm_certificate" "alexcloudlab" {
  domain   = "*.alexcloudlab.org"
  statuses = ["ISSUED"]
  provider = "aws.us"
}

resource "aws_cloudfront_origin_access_identity" "CF-OAI" {
  comment = "hugo sandbox CF-OAI"
}

resource "aws_cloudfront_distribution" "s3_distribution" {
  origin {
    domain_name = "${aws_s3_bucket.hugo-sandbox.bucket_regional_domain_name}"
    origin_id   = "Bucket hugo sandbox"

    s3_origin_config {
      origin_access_identity = "${aws_cloudfront_origin_access_identity.CF-OAI.cloudfront_access_identity_path}"
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "Hugo sandbox"
  default_root_object = "index.html"

  aliases = ["hugo-sandbox.alexcloudlab.org"]

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "Bucket hugo sandbox"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    function_association {
      event_type   = "viewer-request"
      function_arn  = "${aws_cloudfront_function.rewrite-index-html.arn}"
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  price_class = "PriceClass_100"

  restrictions {
      geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    Environment = "production"
  }

  viewer_certificate {
    cloudfront_default_certificate = false
    acm_certificate_arn = data.aws_acm_certificate.alexcloudlab.arn
    minimum_protocol_version = "TLSv1"
    ssl_support_method = "sni-only"
  }
}

resource "aws_cloudfront_function" "rewrite-index-html" {
  name    = "rewrite-index-html"
  runtime = "cloudfront-js-1.0"
  comment = "rewrite '/' to '/index.html'"
  publish = true
  code    = file("rewrite-index-html.js")
}